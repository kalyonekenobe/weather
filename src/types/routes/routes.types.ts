import {ReactElement} from "react";

export type ProtectedRoutePropsType = {
  element: ReactElement
}