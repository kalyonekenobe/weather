export type AuthorizationFormFieldErrorType = {
  fieldName: string,
  errorMessage: string
}

export type AuthorizationFormStateType = {
  data: AuthorizationFormDataType,
  errors: AuthorizationFormFieldErrorType[]
}

export type AuthorizationFormDataType = {
  email: string,
  password: string,
  rememberMe: boolean
}

export type AuthorizationStateType = {
  user?: AuthorizationUserType | null
}

export type AuthorizationUserType = {
  email: string,
  password: string,
  rememberUserForMonth: boolean
}

export type AuthorizationProviderType = {
  user?: AuthorizationUserType | null,
  signIn: any,
  signOut: any
}