export type WeatherDataType = {
  day: Date,
  weatherCode: number,
  minTemperature: number,
  maxTemperature: number,
  humidity: number,
  pressure: number,
  visibility: number,
  windSpeed: number
}

export type DashboardStateType = {
  data: WeatherDataType[],
  location: any,
  isLoading: boolean
}

export type WeatherCodeType = {
  [property: number]: string
}

export const WEATHER_CODES: WeatherCodeType = {
  0: "Clear sky",
  1: "Mainly clear",
  2: "Partly cloudy",
  3: "Overcast",
  45: "Fog",
  48: "Depositing rime fog",
  51: "Light freezing drizzle",
  53: "Moderate freezing drizzle",
  55: "Dense drizzle",
  56: "Light freezing drizzle",
  57: "Dense freezing drizzle",
  61: "Slight rain",
  63: "Moderate rain",
  65: "Heavy rain",
  66: "Light freezing rain",
  67: "Heavy freezing rain",
  71: "Slight snow fall",
  73: "Moderate snow fall",
  75: "Heavy snow fall",
  77: "Snow grains",
  80: "Slight rain shower",
  81: "Moderate rain shower",
  82: "Violent rain shower",
  85: "Slight snow shower",
  86: "Heavy snow shower",
  95: "Thunderstorm",
  96: "Thunderstorm with slight hail",
  99: "Thunderstorm with heavy hail"
}

export const WEATHER_CODES_IMAGES: WeatherCodeType = {
  0: "22.jpg",
  1: "10.jpg",
  2: "00.jpg",
  3: "20.jpg",
  45: "13.jpg",
  48: "13.jpg",
  51: "33.jpg",
  53: "33.jpg",
  55: "33.jpg",
  56: "23.jpg",
  57: "01.jpg",
  61: "01.jpg",
  63: "02.jpg",
  65: "11.jpg",
  66: "12.jpg",
  67: "11.jpg",
  71: "21.jpg",
  73: "21.jpg",
  75: "21.jpg",
  77: "31.jpg",
  80: "01.jpg",
  81: "02.jpg",
  82: "11.jpg",
  85: "21.jpg",
  86: "21.jpg",
  95: "30.jpg",
  96: "30.jpg",
  99: "30.jpg"
}