import {FC, FormEvent, useState} from "react";
import "./styles/authorization.css";
import {
  AuthorizationFormFieldErrorType,
  AuthorizationFormStateType, AuthorizationUserType
} from "../../types/authorization/authorization.types";
import {useAuth} from "../../hooks/useAuth";
import {useNavigate} from "react-router";

const initialState: AuthorizationFormStateType = {
  data: {
    email: "",
    password: "",
    rememberMe: false
  },
  errors: []
}

export const AuthorizationPage: FC = () => {

  const auth = useAuth();
  const navigate = useNavigate();

  const [state, setState] = useState(initialState);

  const submit = (event: FormEvent) => {
    event.preventDefault();
    let errors: AuthorizationFormFieldErrorType[] = [];

    const email = state.data.email;
    const password = state.data.password;
    const rememberMe = state.data.rememberMe;

    if (password !== process.env.REACT_APP_USER_PASSWORD || email !== process.env.REACT_APP_USER_EMAIL) {
      errors = [{
        fieldName: 'password',
        errorMessage: "Incorrect email or password!"
      }, {
        fieldName: 'email',
        errorMessage: ''
      }]
    }

    if (!password.match("[\\w\\d-.:!@#$%^&*()=+_]{8,}")) {
      errors = [{
        fieldName: 'password',
        errorMessage: "Password must have no less than 8 characters and contain only latin letters and symbols: -.:!@#$%^&*()=+_!"
      }]
    }

    if (!email.match("[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}")) {
      errors = [{
        fieldName: 'email',
        errorMessage: "This email does not match the email pattern!"
      }]
    }

    if (errors.length > 0) {
      setState({...state, errors: errors})
      return;
    }

    const user: AuthorizationUserType = {
      email: email,
      password: password,
      rememberUserForMonth: rememberMe
    };
    auth.signIn(user, () => navigate('/dashboard', {replace: true}));
  }

  return (
    <div className={"authorization-page"}>
      <div className={"authorization-form-container"}>
        <header>
          <h3>Sign in to Weather</h3>
        </header>
        <div className={"content"}>
          <form className={"authorization-form form"} onSubmit={event => submit(event)}>
            <div className={"form-group"}>
              <div className={"form-item"}>
                <label htmlFor={"#auth-email"}>Email</label>
                <input className={`${state.errors.find(error => error.fieldName === "email") ? 'error-input' : ''}`}
                       type={"email"} id={"auth-email"} name={"email"} required={true}
                       onChange={event => {
                         setState({...state, data: {...state.data, email: event.target.value}, errors: []});
                       }}
                />
                {
                  state.errors.filter(error => error.fieldName === "email" && error.errorMessage !== '').map((error, key) => (
                    <span key={key} className={"error-message"}>
                      {error.errorMessage}
                    </span>
                  ))
                }
              </div>
            </div>
            <div className={"form-group"}>
              <div className={"form-item"}>
                <label htmlFor={"#auth-password"}>Password</label>
                <input className={`${state.errors.find(error => error.fieldName === "password") ? 'error-input' : ''}`}
                       type={"password"} id={"auth-password"} name={"password"} required={true}
                       onChange={event => {
                         setState({...state, data: {...state.data, password: event.target.value}, errors: []});
                       }}
                />
                {
                  state.errors.filter(error => error.fieldName === "password" && error.errorMessage !== '').map((error, key) => (
                    <span key={key} className={"error-message"}>
                      {error.errorMessage}
                    </span>
                  ))
                }
              </div>
            </div>
            <div className={"form-group"}>
              <div className={"form-item"}>
                <div className={"checkbox-container"}>
                  <input type={"checkbox"} id={"remember-me"} name={"remember-me"} checked={state.data.rememberMe}
                         onChange={() => {
                           setState({...state, data: {...state.data, rememberMe: !state.data.rememberMe}})
                         }}
                  />
                  <label htmlFor={"remember-me"}>Remember me</label>
                </div>
              </div>
            </div>
            <div className={"form-group"}>
              <div className={"form-item submit-container"}>
                <input type={"submit"} className={"button blue-button"} value={"Sign in"} />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}