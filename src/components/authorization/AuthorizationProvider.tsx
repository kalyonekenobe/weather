import {createContext, FC} from "react";
import {useAuthProvider} from "../../hooks/useAuthProvider";
import {AuthorizationProviderType} from "../../types/authorization/authorization.types";

const context: AuthorizationProviderType = {
  user: null,
  signIn: () => {},
  signOut: () => {}
}

export const authorizationContext = createContext(context);

export const AuthorizationProvider: FC<any> = ({ children }) => {
  const auth = useAuthProvider();

  return (
    <authorizationContext.Provider value={auth}>
      {children}
    </authorizationContext.Provider>
  );
}