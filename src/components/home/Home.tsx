import {FC, useEffect, useState} from "react";
import './styles/home.css'
import {Link} from "react-router-dom";
import {useAuth} from "../../hooks/useAuth";
import {useNavigate} from "react-router";
import {DashboardStateType, WEATHER_CODES, WEATHER_CODES_IMAGES, WeatherDataType} from "../../types/home/home.types";

const initialState: DashboardStateType = {
  data: [],
  location: {},
  isLoading: true
}

export const Home: FC = () => {
  const auth = useAuth();
  const navigate = useNavigate();
  const [state, setState] = useState(initialState);

  const sum = (array: any[], begin: number, end: number) => {
    let sum = 0;
    for (let i = Math.max(0, begin); i < Math.min(array.length, end); i++)
      sum += array[i];
    return sum;
  }

  const min = (array: any[], begin: number, end: number) => {
    let min = 1000;
    for (let i = Math.max(0, begin); i < Math.min(array.length, end); i++)
      min = Math.min(min, array[i]);
    return min;
  }

  const max = (array: any[], begin: number, end: number) => {
    let max = -1000;
    for (let i = Math.max(0, begin); i < Math.min(array.length, end); i++)
      max = Math.max(max, array[i]);
    return max;
  }

  const normalizeWeatherData = (weather: any): WeatherDataType[] => {
    const daily = weather.daily;
    const hourly = weather.hourly;
    let normalizedData: WeatherDataType[] = [];
    for (let i = 0; i < 5; i++) {
      const data: WeatherDataType = {
        day: new Date(daily.time[i] * 1000),
        weatherCode: daily.weathercode[i],
        minTemperature: min(hourly.apparent_temperature, i * 24, i * 24 + 24),
        maxTemperature: max(hourly.apparent_temperature, i * 24, i * 24 + 24),
        humidity: Math.round(sum(hourly.relativehumidity_2m, i * 24, i * 24 + 24) / 24),
        pressure: Math.round(sum(hourly.surface_pressure, i * 24, i * 24 + 24) / 24),
        visibility: parseFloat((sum(hourly.visibility, i * 24, i * 24 + 24) / 24 / 1000).toFixed(2)),
        windSpeed: Math.round(sum(hourly.windspeed_10m, i * 24, i * 24 + 24) / 24)
      }
      normalizedData = [...normalizedData, data]
    }
    return normalizedData
  }

  const fetchWeatherData = () => {
    setState({...state, isLoading: true})
    navigator.geolocation.getCurrentPosition(async position => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
      const timezoneApiUrl = `https://api.geoapify.com/v1/geocode/reverse?lat=${latitude}&lon=${longitude}&format=json&apiKey=cc8044647eb64f1b93c693d5a597da41`;
      const locationData = await fetch(timezoneApiUrl).then(async response => await response.json());
      const timezoneName = locationData.results[0].timezone.name;
      let weatherApiUrl = `https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&hourly=apparent_temperature,windspeed_10m,relativehumidity_2m,surface_pressure,visibility&daily=weathercode,temperature_2m_max,temperature_2m_min&timezone=${timezoneName}&timeformat=unixtime`
      const weather = await fetch(weatherApiUrl).then(async response => await response.json());
      setState({...state, data: normalizeWeatherData(weather), location: locationData.results[0], isLoading: false});
    })
  }

  useEffect(() => {
    fetchWeatherData();
  }, [])

  return (
    <main className={"main-container"}>
      <header className={"page-header"}>
        <h1>Weather</h1>
        <nav>
          <Link to={'/dashboard'} replace={true}>Dashboard</Link>
          <Link to={'/api'} replace={true}>API</Link>
          <Link to={'/about'} replace={true}>About</Link>
          <a onClick={() => auth.signOut(() => navigate('/sign-in', {replace: true}))}>Logout</a>
        </nav>
      </header>
      <div className={"content"}>
        <header>
          <h2>
            {state.location.city}, {state.location.country}
          </h2>
        </header>
        <div className={"weather-dashboard"}>
          {
            !state.isLoading ? state.data.map((data, key) => (
              <div key={key} className={"weather-card"}>
                <header>
                  <h3>
                    {
                      data.day.toLocaleDateString("en-US", {
                        weekday: 'short', day: "2-digit", month: 'short'
                      })
                    }
                  </h3>
                  <img src={`${process.env.PUBLIC_URL}/images/${WEATHER_CODES_IMAGES[data.weatherCode]}`}
                       alt={WEATHER_CODES[data.weatherCode]}
                  />
                  <p>{WEATHER_CODES[data.weatherCode]}</p>
                </header>
                <div className={"content"}>
                  <p><span>Min:</span> {data.minTemperature} °C</p>
                  <p><span>Max:</span> {data.maxTemperature} °C</p>
                  <p><span>Wind speed:</span> {data.windSpeed} km/h</p>
                  <p><span>Humidity:</span> {data.humidity} %</p>
                  <p><span>Visibility:</span> {data.visibility} km</p>
                  <p><span>Pressure:</span> {data.pressure} hPa</p>
                </div>
              </div>
            ))
              :
              <div className={"loading-container"}>
                <img src={`${process.env.PUBLIC_URL}/images/loadinggif.webp`} alt={"Loading"} />
              </div>
          }
          <a className={"button blue-button refresh"} onClick={() => fetchWeatherData()}>Refresh weather data</a>
        </div>
      </div>
    </main>
  );
}