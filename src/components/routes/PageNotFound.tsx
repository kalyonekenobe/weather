import {FC} from "react";
import {Link} from "react-router-dom";

export const PageNotFound: FC = () => {

  return (
    <main className={"main-container page-not-found"}>
      <div className={"content"}>
        <img src={`${process.env.PUBLIC_URL}/images/not-found.jpg`} alt="Page was not found" />
        <div className={"info"}>
          <h1>404</h1>
          <h2>Page not found</h2>
          <p>We are sorry, the page you requested could not be found. Please go back to the homepage.</p>
          <Link to={'/dashboard'} replace={true} className={'button blue-button'}>Go home</Link>
        </div>
      </div>
    </main>
  );
}