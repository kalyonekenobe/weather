import {FC} from "react";
import { Navigate } from "react-router";
import {useAuth} from "../../hooks/useAuth";
import {ProtectedRoutePropsType} from "../../types/routes/routes.types";

export const ProtectedRoute: FC<ProtectedRoutePropsType> = (props) => {
  const auth = useAuth();

  return auth.user ? props.element : <Navigate to={"/sign-in"} replace />;
}