import React, {FC} from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import {AuthorizationPage} from "../components/authorization/AuthorizationPage";
import {ProtectedRoute} from "../components/routes/ProtectedRoute";
import {Home} from "../components/home/Home";
import {PageNotFound} from "../components/routes/PageNotFound";

const App: FC = () => {
  return (
    <div className="app">
      <BrowserRouter>
        <Routes>
          <Route index path={'/sign-in'} element={<AuthorizationPage />} />
          <Route path={'/dashboard'} element={<ProtectedRoute element={<Home />} />} />
          <Route path={'*'} element={<ProtectedRoute element={<PageNotFound />} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;