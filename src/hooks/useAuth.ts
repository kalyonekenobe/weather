import {useContext} from "react";
import {authorizationContext} from "../components/authorization/AuthorizationProvider";

export const useAuth = () => useContext(authorizationContext);