import {useState} from "react";
import {
  AuthorizationProviderType,
  AuthorizationStateType,
  AuthorizationUserType
} from "../types/authorization/authorization.types";
import {decodeJwtToken, generateJwtToken, isJwtTokenValid} from "../helpers/jwtHandler";
import {JWTPayload} from "jose";
import {clearCookie, getCookie, setCookie} from "../helpers/cookieHandler";

const jwtInCookies = getCookie("accessToken");

const initialState: AuthorizationStateType = {
  user: (jwtInCookies && isJwtTokenValid(jwtInCookies) ? decodeJwtToken(jwtInCookies) : null) as AuthorizationUserType
}

export const useAuthProvider = (): AuthorizationProviderType => {
  const [state, setState] = useState(initialState);

  const signIn = async (user: AuthorizationUserType, callback: any) => {
    const jwtPayload: JWTPayload = {
      iss: process.env.REACT_APP_JWT_ISSUER,
      aud: process.env.REACT_APP_JWT_AUDIENCE,
      exp: user.rememberUserForMonth ? Math.floor(new Date().getTime() / 1000) + 30 * 24 * 60 * 60 : undefined,
      user: user
    }
    const jwt = await generateJwtToken(jwtPayload)
    setCookie("accessToken", jwt);
    setState({...state, user: user});
    callback();
  }

  const signOut = async (callback: any) => {
    setState({...state, user: null});
    clearCookie("accessToken");
    callback();
  }

  return {
    user: state.user,
    signIn: signIn,
    signOut: signOut
  } as AuthorizationProviderType
}