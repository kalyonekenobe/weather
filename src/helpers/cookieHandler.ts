export const setCookie = (name: string, value: string, expires?: number) => {
  document.cookie = `${name}=${value};${expires ? `expires: ${expires.toString()}` : ''}`;
}

export const getCookie = (name: string): string | undefined => {
  name = `${name}=`;
  let decodedCookie = decodeURIComponent(document.cookie);
  let cookiesList = decodedCookie.split(';');
  for (let i = 0; i < cookiesList.length; i++) {
    let cookie = cookiesList[i];
    cookie = cookie.trim();
    if (cookie.indexOf(name) === 0)
      return cookie.substring(name.length, cookie.length);
  }
}

export const clearCookie = (name: string) => {
  name = `${name}=`;
  let decodedCookie = decodeURIComponent(document.cookie);
  let cookiesList = decodedCookie.split(';');
  for (let i = 0; i < cookiesList.length; i++) {
    let cookie = cookiesList[i];
    cookie = cookie.trim();
    if (cookie.indexOf(name) === 0)
      document.cookie = name + cookie.substring(name.length, cookie.length) + ";expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
}