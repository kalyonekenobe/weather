import * as jose from 'jose';
import {decodeJwt, JWTPayload} from "jose";

export const generateJwtToken = async (payload: JWTPayload) => {
  const alg = 'HS256';
  const secret = new TextEncoder().encode(process.env.REACT_APP_JWT_SECRET);
  return await new jose.SignJWT(payload)
    .setProtectedHeader({ alg })
    .setIssuedAt()
    .setIssuer(payload.iss ?? '')
    .setAudience(payload.aud ?? '')
    .setExpirationTime(payload.exp ?? '10m')
    .sign(secret);
}

export const isJwtTokenValid = (jwt: string): boolean => {
  const token = decodeJwt(jwt);
  return token.exp ? token.exp >= Math.floor(new Date().getTime() / 1000) : true
}

export const decodeJwtToken = (jwt: string): JWTPayload => decodeJwt(jwt);